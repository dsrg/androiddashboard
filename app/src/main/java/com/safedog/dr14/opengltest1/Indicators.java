package com.safedog.dr14.opengltest1;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.safedog.dr14.opengltest1.helper.RawResourceReader;
import com.safedog.dr14.opengltest1.helper.ShaderHelper;
import com.safedog.dr14.opengltest1.helper.TextureHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Mat78 on 02.05.2017.
 */

public class Indicators {



    /** Store our model data in a float buffer. */
    private final FloatBuffer mCubePositions;
    private final FloatBuffer mCubeNormals;
    private final FloatBuffer mCubeTextureCoordinates;
    private final Context mObjectContext;
    private int mTextureDataHandle;

    /** This will be used to pass in the transformation matrix. */
    private int mMVPMatrixHandle;
    /** This will be used to pass in the modelview matrix. */
    private int mMVMatrixHandle;
    /** This will be used to pass in the texture. */
    private int mTextureUniformHandle;
    /** This will be used to pass in model position information. */
    private int mPositionHandle;
    /** This will be used to pass in model normal information. */
    private int mNormalHandle;
    /** This will be used to pass in model texture coordinate information. */
    private int mTextureCoordinateHandle;
    /** This is a handle to our cube shading program. */
    private int mProgramHandle;
    /**
     * Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
     * it positions things relative to our eye.
     */
    private float[] mViewMatrix = new float[16];
    /** How many bytes per float. */
    private final int mBytesPerFloat = 4;
    /** Size of the position data in elements. */
    private final int mPositionDataSize = 3;
    /** Size of the color data in elements. */
    private final int mColorDataSize = 4;
    /** Size of the normal data in elements. */
    private final int mNormalDataSize = 3;
    /** Size of the texture coordinate data in elements. */
    private final int mTextureCoordinateDataSize = 2;

    private float[] mModelMatrix = new float[16];

    private float[] mMVPMatrix = new float[16];

    private int mTextureDataHandle_blinkrPravy;
    private int mTextureDataHandle_blinkrLevy;
    private int mTextureDataHandle_handBrake;

    private int mTextureDataHandle_ignitionState;
    private int mTextureDataHandle_dalkovaSvetla;
    private int mTextureDataHandle_parkLight;
    private int mTextureDataHandle_absCerveny;
    private int mTextureDataHandle_absZluty;
    private int mTextureDataHandle_EDC;
    private int mTextureDataHandle_oil;
    private int mTextureDataHandle_engine;
    private int mTextureDataHandle_fogLight;
    private int mTextureDataHandle_fogLightRear;
    private int mTextureDataHandle_lowLamp;

    protected String getVertexShader()
    {
        return RawResourceReader.readTextFileFromRawResource(mObjectContext, R.raw.per_pixel_vertex_shader_tex_and_light);
    }

    protected String getFragmentShader()
    {
        return RawResourceReader.readTextFileFromRawResource(mObjectContext, R.raw.per_pixel_fragment_shader_tex_and_light);
    }

    public Indicators(final Context GaugeContext){
        mObjectContext = GaugeContext;

        // Define points for a cube.

        // X, Y, Z
        final float[] cubePositionData =
                {
                        // In OpenGL counter-clockwise winding is default. This means that when we look at a triangle,
                        // if the points are counter-clockwise we are looking at the "front". If not we are looking at
                        // the back. OpenGL has an optimization where all back-facing triangles are culled, since they
                        // usually represent the backside of an object and aren't visible anyways.

                        // Front face
                        -0.23697916666666666666666666666667f, 0.23697916666666666666666666666667f, 1.0f,
                        -0.23697916666666666666666666666667f, -0.23697916666666666666666666666667f, 1.0f,
                        0.23697916666666666666666666666667f, 0.23697916666666666666666666666667f, 1.0f,
                        -0.23697916666666666666666666666667f, -0.23697916666666666666666666666667f, 1.0f,
                        0.23697916666666666666666666666667f, -0.23697916666666666666666666666667f, 1.0f,
                        0.23697916666666666666666666666667f, 0.23697916666666666666666666666667f, 1.0f,




                };

        // X, Y, Z
        // The normal is used in light calculations and is a vector which points
        // orthogonal to the plane of the surface. For a cube model, the normals
        // should be orthogonal to the points of each face.
        final float[] cubeNormalData =
                {
                        // Front face
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,


                };

        // S, T (or X, Y)
        // Texture coordinate data.
        // Because images have a Y axis pointing downward (values increase as you move down the image) while
        // OpenGL has a Y axis pointing upward, we adjust for that here by flipping the Y axis.
        // What's more is that the texture coordinates are the same for every face.
        final float[] cubeTextureCoordinateData =
                {
                        // Front face
                        0.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,


                };

        // Initialize the buffers.
        mCubePositions = ByteBuffer.allocateDirect(cubePositionData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubePositions.put(cubePositionData).position(0);


        mCubeNormals = ByteBuffer.allocateDirect(cubeNormalData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeNormals.put(cubeNormalData).position(0);

        mCubeTextureCoordinates = ByteBuffer.allocateDirect(cubeTextureCoordinateData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeTextureCoordinates.put(cubeTextureCoordinateData).position(0);

        // Position the eye in front of the origin.
        final float eyeX = 0.0f;
        final float eyeY = 0.0f;
        final float eyeZ = -0.5f;

        // We are looking toward the distance
        final float lookX = 0.0f;
        final float lookY = 0.0f;
        final float lookZ = -5.0f;

        // Set our up vector. This is where our head would be pointing were we holding the camera.
        final float upX = 0.0f;
        final float upY = 1.0f;
        final float upZ = 0.0f;

        // Set the view matrix. This matrix can be said to represent the camera position.
        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
        // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

        final String vertexShader = getVertexShader();
        final String fragmentShader = getFragmentShader();

        final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);
        final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);

        mProgramHandle = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,
                new String[] {"a_Position",  "a_Color", "a_Normal", "a_TexCoordinate"});

        mTextureDataHandle_blinkrLevy = TextureHelper.loadTexture(mObjectContext, R.drawable.leftblinkr);

        mTextureDataHandle_blinkrPravy = TextureHelper.loadTexture(mObjectContext, R.drawable.rightblinkr);

        mTextureDataHandle_handBrake = TextureHelper.loadTexture(mObjectContext, R.drawable.kont9_1080);

        mTextureDataHandle_ignitionState = TextureHelper.loadTexture(mObjectContext, R.drawable.kont4_1080);

        mTextureDataHandle_dalkovaSvetla = TextureHelper.loadTexture(mObjectContext, R.drawable.kont10_1080);

        mTextureDataHandle_absCerveny = TextureHelper.loadTexture(mObjectContext, R.drawable.kont5_1080);

        mTextureDataHandle_absZluty = TextureHelper.loadTexture(mObjectContext, R.drawable.kont7_1080);

        mTextureDataHandle_EDC = TextureHelper.loadTexture(mObjectContext, R.drawable.kont8_1080);

        mTextureDataHandle_oil = TextureHelper.loadTexture(mObjectContext, R.drawable.oil);

        mTextureDataHandle_engine = TextureHelper.loadTexture(mObjectContext, R.drawable.engine);

        mTextureDataHandle_fogLight = TextureHelper.loadTexture(mObjectContext, R.drawable.fog_lamp);

        mTextureDataHandle_fogLightRear = TextureHelper.loadTexture(mObjectContext, R.drawable.fog_lamp_rear);

        mTextureDataHandle_lowLamp = TextureHelper.loadTexture(mObjectContext, R.drawable.low_lamp);
    }

    /**
     * Draws a cube.
     */
    public void drawCube(float[] mProjectionMatrix, float x, float y, float z, String indikator)
    {

        // Set our per-vertex lighting program.
        GLES20.glUseProgram(mProgramHandle);

        // Set program handles for cube drawing.
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVMatrix");
        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_Texture");
        mPositionHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");
        mNormalHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Normal");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");

        if(indikator.equals("lowLamp")){
            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_lowLamp);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_lowLamp, 0);
        }


        if(indikator.equals("fogLight")){
            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_fogLight);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_fogLight, 0);
        }

        if(indikator.equals("fogLightRear")){
            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_fogLightRear);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_fogLightRear, 0);
        }

        if(indikator.equals("leftBlinkr")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_blinkrLevy);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_blinkrLevy, 0);

        }else if(indikator.equals("rightBlinkr")){

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_blinkrPravy);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_blinkrPravy, 0);

        }else if(indikator.equals("handBrake")){

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_handBrake);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_handBrake, 0);

        }else if(indikator.equals("dalkovaSvetla")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_dalkovaSvetla);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_dalkovaSvetla, 0);
        }else if(indikator.equals("ignition")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_ignitionState);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_ignitionState, 0);
        }else if(indikator.equals("oil")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_oil);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_oil, 0);
        }else if(indikator.equals("engine")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_engine);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_engine, 0);
        }else if(indikator.equals("absc")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_absCerveny);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_absCerveny, 0);
        }else if(indikator.equals("absz")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_absZluty);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_absZluty, 0);
        }else if(indikator.equals("edc")) {

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle_EDC);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureDataHandle_EDC, 0);
        }

        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.translateM(mModelMatrix, 0, x, y, z);

        //Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 0.0f, 1.0f);

        // Pass in the position information
        mCubePositions.position(0);
        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
                0, mCubePositions);

        GLES20.glEnableVertexAttribArray(mPositionHandle);


        // Pass in the normal information
        mCubeNormals.position(0);
        GLES20.glVertexAttribPointer(mNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false,
                0, mCubeNormals);

        GLES20.glEnableVertexAttribArray(mNormalHandle);

        // Pass in the texture coordinate information
        mCubeTextureCoordinates.position(0);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false,
                0, mCubeTextureCoordinates);

        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

        // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);

        // Pass in the modelview matrix.
        GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);

        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);

        // Pass in the combined matrix.
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);


        // Draw the cube.
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);
    }


}