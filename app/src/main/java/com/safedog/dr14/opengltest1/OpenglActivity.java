package com.safedog.dr14.opengltest1;

/**
 * Created by Mat78 on 05.06.2017.
 */

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ConfigurationInfo;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;


public class OpenglActivity extends Activity  {

    //UdpClientHandler udpClientHandler;

    /** Hold a reference to our GLSurfaceView */
    private GLSurfaceView mGLSurfaceView;
    String Address;
    int Port;


    public OpenglActivity(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Bundle extras = getIntent().getExtras();


          Address = extras.getString("adress");
           Port = extras.getInt("port");
      //  if(UdpThread.running){

       // }

       // UdpThread.running = true;




        super.onCreate(savedInstanceState);

        mGLSurfaceView = new GLSurfaceView(this);

   //     View decorView = getWindow().getDecorView();
// Hide the status bar.
      //  int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
       // decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
      //  ActionBar actionBar = getActionBar();
       // actionBar.hide();






        // Check if the system supports OpenGL ES 2.0.
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

        if (supportsEs2)
        {
            // Request an OpenGL ES 2.0 compatible context.
            mGLSurfaceView.setEGLContextClientVersion(2);

            // Set the renderer to our demo renderer, defined below.
            mGLSurfaceView.setRenderer(new MyRenderer(this,Address,Port));

        }
        else
        {
            // This is where you could create an OpenGL ES 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2.
            return;
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
      /*  View v = this.getWindow().getDecorView();
        v.setSystemUiVisibility(View.GONE);*/

        setContentView(mGLSurfaceView);


    }



    int min_delay = 200;
    long last_clicked = 0;



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        int a =mGLSurfaceView.getRight();
        int b = mGLSurfaceView.getBottom();

        long delta = SystemClock.uptimeMillis() - last_clicked;

       // final MediaPlayer mp = MediaPlayer.create(this, R.raw.button);


        if(delta > min_delay){

            if (x > (a/2.5566f) && x < (a/2) && y > b/1.39355f && y < b/1.269095f) {
                TextView.Tlacitko++;
                //mp.start();
                if (TextView.Tlacitko == 3) {
                    TextView.Tlacitko = 0;
                }

            }

            if (x > (a/2) && x < (a/1.63127f) && y > b/1.39355f && y < b/1.269095f) {
                if(TextView.acc ==0){
                    TextView.acc = 1;
                }else{
                    TextView.acc = 0;
                }

                //mp.start();
            }

            if (x > a/1.094017f && x < a/1.058726f && y > b/30.08f && y < b/12.32787f) {


                //start settings activity
                Intent SettingsActivity = new Intent(getApplicationContext(),SettingsActivity.class);
                startActivity(SettingsActivity);

                //mp.start();
            }
        }

        x = 0;
        y = 0;

        last_clicked = SystemClock.uptimeMillis();
        return true;
    }

    @Override
    protected void onResume()
    {
        // The activity must call the GL surface view's onResume() on activity onResume().
        super.onResume();
        mGLSurfaceView.onResume();
    }

    @Override
    protected void onPause()
    {
        // The activity must call the GL surface view's onPause() on activity onPause().
        super.onPause();
        mGLSurfaceView.onPause();
    }
}
