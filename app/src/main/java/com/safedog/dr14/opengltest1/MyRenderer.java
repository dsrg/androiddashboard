package com.safedog.dr14.opengltest1;


import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.lang.Math.abs;

/**
 * Created by dr14 on 4/22/2017.
 */

public class MyRenderer  implements GLSurfaceView.Renderer {





    /** Used for debug logs. */
    private static final String TAG = "LessonFourRenderer";

    private final Context mActivityContext;

    /** Store the projection matrix. This is used to project the scene onto a 2D viewport. */
    private float[] mProjectionMatrix = new float[16];

    /**
     * Stores a copy of the model matrix specifically for the light position.
     */
    private float[] mLightModelMatrix = new float[16];

    /**
     * Initialize the model data.
     */

    static int settingsHelper = 0;

    private int Layout;

    private int getLayout(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivityContext);
        int mLayout =  Integer.parseInt(prefs.getString("layout_preference","0"));
        return mLayout;
    }

    public MyRenderer(final Context activityContext,String Address,int Port)
    {
        mActivityContext = activityContext;
        this.Address = Address;
        this.Port = Port;

        Layout = getLayout();

        if(UdpThread.running == false) {

            if(settingsHelper == 0){
                udpThread = new UdpThread(Address, Port, activityContext);
            }

                udpThread.start();
        }


        udpThread.running = true;
        settingsHelper++;
    }





    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config)
    {

        // Set the background clear color to black.
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        mBackground = new Background(mActivityContext);
        mBackground1 = new Background1(mActivityContext);
        mNeedle = new Needle(mActivityContext);
        mNeedle_mala = new Needle_mala(mActivityContext);
        mIndicators = new Indicators(mActivityContext);
        mText = new TextView(mActivityContext);
        mSettings = new SettingsRec(mActivityContext);

        // Use culling to remove back faces.
        GLES20.glEnable(GLES20.GL_CULL_FACE);

        // Enable depth testing
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
       // Transperancy
        GLES20.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        // Enable blending
        GLES20.glEnable(GL10.GL_BLEND);
        // The below glEnable() call is a holdover from OpenGL ES 1, and is not needed in OpenGL ES 2.
        // Enable texture mapping
        // GLES20.glEnable(GLES20.GL_TEXTURE_2D);



    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height)
    {
        // Set the OpenGL viewport to the same size as the surface.
        GLES20.glViewport(0, 0, width, height);

        // Create a new perspective projection matrix. The height will stay the same
        // while the width will vary as per aspect ratio.
        final float ratio = (float) width / height;
        final float left = -ratio;
        final float right = ratio;
        final float bottom = -1.0f;
        final float top = 1.0f;
        final float near = 1.0f;
        final float far = 10.0f;

        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);



    }

   // private TextManager mTextManager;

    private SettingsRec mSettings;
    private TextView mText;
    private Background mBackground;
    private Background1 mBackground1;
    private Needle mNeedle;
    private Needle_mala mNeedle_mala;
    private Indicators mIndicators;
    int TestFrame_num = 150;
    String Address;
    int Port;

    int Speed;
    int Rpm;
    int fuelLevel;
    int temperature;
    int voltage;

    UdpThread udpThread;



    @Override
    public void onDrawFrame(GL10 glUnused)
    {
        Layout = getLayout();
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);


        blinkrL = udpThread.getLeftBlinkr();
        blinkrR = udpThread.getRightBlinkr();
        engineOn = udpThread.getEngineOn();
        starterState = udpThread.getStarterState();
        ignitionState = udpThread.getIgnitionState();
        headLamp = udpThread.getHeadLamp();
        lowLamp = udpThread.getLowLamp();
        handBreak = udpThread.getHandBreak();
        fog_light = udpThread.getFogLight();
        fog_light_rear = udpThread.getFogLightRear();
        park_light = udpThread.getParkLight();
// Do a complete rotation every 10 seconds.
      //  long time = SystemClock.uptimeMillis() % 10000L;
      //  float angleInDegrees = (360.0f / 10000.0f) * ((int) time);

        //background
        if(Layout == 0) {
            mBackground.drawCube(mProjectionMatrix,1);


            if (udpThread.getOilLevel() < 20) {
                //engine indicator
                mIndicators.drawCube(mProjectionMatrix, 3.75f, -0.5f, -4.95f, "oil");
            }

            if (engineOn || starterState || ignitionState) {
                //engine indicator
                mIndicators.drawCube(mProjectionMatrix, 3.75f, -0.1f, -4.95f, "engine");
            }
            if (starterState || ignitionState) {
                //EDC
                mIndicators.drawCube(mProjectionMatrix, 4.2f, -0.1f, -4.95f, "edc");
                //ignition baterka
                mIndicators.drawCube(mProjectionMatrix, 2.75f, -0.1f, -4.95f, "ignition");
                //handbrake
                mIndicators.drawCube(mProjectionMatrix, 2.3f, -0.1f, -4.95f, "handBrake");
                //low lamp
                mIndicators.drawCube(mProjectionMatrix, 0f, 1.6102f, -4.9f, "lowLamp");
            }

            if (handBreak) {
                //handbrake
                mIndicators.drawCube(mProjectionMatrix, 2.3f, -0.1f, -4.95f, "handBrake");
            }

            if(fog_light){
                mIndicators.drawCube(mProjectionMatrix, -3.75f, -0.1f, -4.95f, "fogLight");
            }

            if(fog_light_rear){
                mIndicators.drawCube(mProjectionMatrix, -4.2f, -0.1f, -4.95f, "fogLightRear");
            }



            String text;

            if (TextView.Tlacitko == 1) {
                text = "CONSUMPTION";


            } else if (TextView.Tlacitko == 2) {
                text = "DISTANCE";


            } else {
                text = "GEAR";

            }

            char[] letters = text.toCharArray();
            int length1 = text.length();

            float letterOffsetx = 0.11f;
            float letterOffsety = 0.1f;


            for (int i = 0; i < length1; i++) {
                //text
                mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.8f, letters[i], 1f);
            }

            if (TextView.Tlacitko == 0) {
                int gear = udpThread.getGear();
                String valuetext = "" + gear;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }

            if (TextView.Tlacitko == 2) {
                float distance = udpThread.getDistance();
                String valuetext = "" + distance;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }

            if (TextView.Tlacitko == 1) {
                float consumtion = udpThread.getFuelCons();
                String valuetext = "" + consumtion;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }


            SimpleDateFormat hod = new SimpleDateFormat("HH:mm");
            String time = hod.format(Calendar.getInstance().getTime());

            int length2 = time.length();

            for (int i = 0; i < length2; i++) {
                //text
                mText.drawCube(mProjectionMatrix, 0.0f - (2 * letterOffsetx) + (i * letterOffsetx), 1.2f, time.toCharArray()[i], 1.0f);
            }

            if (TextView.acc == 1) {

                String ccState = "CRUISE CONTROL ON";
                char[] letter = ccState.toCharArray();
                int length3 = ccState.length();


                for (int i = 0; i < length3; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.85f + (i * letterOffsetx), -0.82f, letter[i], 1f);
                }

            }


            //needle otacky
            Rpm = udpThread.getRpm();
            SetValueOtacky(Rpm);
            mNeedle.drawCube(mProjectionMatrix, angleConO, -3.1687475f, -0.1f, -4.9f);

            //draw needle rychlost
            Speed = udpThread.getSpeed();
            SetValueRychlost(Speed); //x
            mNeedle.drawCube(mProjectionMatrix, angleConR, 3.1687475f, -0.1f, -4.9f);

            //draw needle temperature
            temperature = udpThread.getTemperature();
            SetValueHl(temperature);
            mNeedle_mala.drawCube(mProjectionMatrix, angleConH, -3.08f, -1.1848148148148148148148148148148f, -4.8f);

            //draw needle fuel
            fuelLevel = udpThread.getFuelLevel();
            SetValueBenzin(fuelLevel);
            mNeedle_mala.drawCube(mProjectionMatrix, angleConB, 3.08f, -1.1848148148148148148148148148148f, -4.8f);
        }

        if(Layout == 1){

            mBackground.drawCube(mProjectionMatrix,2);
            mBackground1.drawCube(mProjectionMatrix, -3.238f, 0.0f, -4.99f, 1);
            mBackground1.drawCube(mProjectionMatrix, 3.238f, 0.0f, -4.99f, 2);

            if (udpThread.getOilLevel() < 20) {
                //engine indicator
                mIndicators.drawCube(mProjectionMatrix, 3.75f, -0.4f, -4.95f, "oil");
            }

            if (engineOn || starterState || ignitionState) {
                //engine indicator
                mIndicators.drawCube(mProjectionMatrix, 3.75f, 0.0f, -4.95f, "engine");
            }
            if (starterState || ignitionState) {
                //EDC
                mIndicators.drawCube(mProjectionMatrix, 4.2f, 0f, -4.95f, "edc");
                //ignition baterka
                mIndicators.drawCube(mProjectionMatrix, 2.75f, -0.1f, -4.95f, "ignition");
                //handbrake
                mIndicators.drawCube(mProjectionMatrix, 2.75f, 0.29f, -4.95f, "handBrake");
                //low lamp
                mIndicators.drawCube(mProjectionMatrix, 0f, 1.6102f, -4.9f, "lowLamp");
            }


            if (handBreak) {
                //handbrake
                mIndicators.drawCube(mProjectionMatrix, 2.75f, 0.29f, -4.95f, "handBrake");
            }

            if(fog_light){
                mIndicators.drawCube(mProjectionMatrix, -3.75f, -0.1f, -4.95f, "fogLight");
            }

            if(fog_light_rear){
                mIndicators.drawCube(mProjectionMatrix, -3.75f, 0.29f, -4.95f, "fogLightRear");
            }

            String text;

            if (TextView.Tlacitko == 1) {
                text = "CONSUMPTION";


            } else if (TextView.Tlacitko == 2) {
                text = "DISTANCE";


            } else {
                text = "GEAR";

            }

            char[] letters = text.toCharArray();
            int length1 = text.length();

            float letterOffsetx = 0.11f;
            float letterOffsety = 0.1f;


            for (int i = 0; i < length1; i++) {
                //text
                mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.8f, letters[i], 1f);
            }

            if (TextView.Tlacitko == 0) {
                int gear = udpThread.getGear();
                String valuetext = "" + gear;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }

            if (TextView.Tlacitko == 2) {
                float distance = udpThread.getDistance();
                String valuetext = "" + distance;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }

            if (TextView.Tlacitko == 1) {
                consumption = udpThread.getFuelCons();
                String valuetext = "" + consumption;

                int length2 = valuetext.length();

                for (int i = 0; i < length2; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.68f + (i * letterOffsetx), 0.5f, valuetext.toCharArray()[i], 1f);
                }
            }


            SimpleDateFormat hod = new SimpleDateFormat("HH:mm");
            String time = hod.format(Calendar.getInstance().getTime());

            int length2 = time.length();

            for (int i = 0; i < length2; i++) {
                //text
                mText.drawCube(mProjectionMatrix, 0.0f - (2 * letterOffsetx) + (i * letterOffsetx), 1.2f, time.toCharArray()[i], 1.0f);
            }

            if (TextView.acc == 1) {

                String ccState = "CRUISE CONTROL ON";
                char[] letter = ccState.toCharArray();
                int length3 = ccState.length();


                for (int i = 0; i < length3; i++) {
                    //text
                    mText.drawCube(mProjectionMatrix, -0.85f + (i * letterOffsetx), -0.82f, letter[i], 1f);
                }

            }


            //needle otacky

            consumption = udpThread.getFuelCons();
            SetValueConsumption(consumption);
            mNeedle.drawCube(mProjectionMatrix, angleConO, -3.1f, 0f, -4.9f);

            //draw needle rychlost
            Speed = udpThread.getSpeed();
            SetValueRychlost1(Speed); //x
            mNeedle.drawCube(mProjectionMatrix, angleConR, 3.1f, 0f, -4.9f);

            //draw needle voltage
            voltage = udpThread.getVoltage();
            SetValueVoltage(voltage);
            mNeedle_mala.drawCube(mProjectionMatrix, angleConH, -3.05f, -1.05f, -4.8f);

            //draw needle fuel
            fuelLevel = udpThread.getFuelLevel();
            SetValueBenzin1(fuelLevel);
            mNeedle_mala.drawCube(mProjectionMatrix, angleConB, 3.05f, -1.05f, -4.8f);
        }


        //blinkry
        long time2 = SystemClock.uptimeMillis() % 1000L;
        int state = 1 * ((int) time2);
        BlinkrOnOff(state);



        if(blinkrL) {
            //draw blinkr levy
            mIndicators.drawCube(mProjectionMatrix, -1.517292f, 1.6202f, BlinkrZ, "leftBlinkr");

        }
        if(blinkrR) {
            //draw blinkr pravy
            mIndicators.drawCube(mProjectionMatrix, 1.537292f, 1.6102f, BlinkrZ, "rightBlinkr");
        }

        if(headLamp) {
            //dalkova svetla
            mIndicators.drawCube(mProjectionMatrix, -0.7f, 1.6f, -4.9f, "dalkovaSvetla");
        }

        if(lowLamp){
            mIndicators.drawCube(mProjectionMatrix, 0f, 1.6102f, -4.9f, "lowLamp");

        }

        //Draw settings button
        mSettings.drawCube(mProjectionMatrix,5, 3, -4.9f,1);

        horn = udpThread.getHorn();
/*
        MediaPlayer mp = MediaPlayer.create(mActivityContext, R.raw.horn);
        long delta = SystemClock.uptimeMillis() - last_clicked;


        if(horn & delta > min_delay){

            //mp.start();
            last_clicked = SystemClock.uptimeMillis();
            mp = null;
        }


        int reverse = udpThread.getGear();
        if(reverse == 0){
            if(prevGear != 0){
                MediaPlayer mpBeep = MediaPlayer.create(mActivityContext, R.raw.beep);
                //mpBeep.start();
            }

        }
        prevGear = reverse;
        */
        loop++;
        if(loop >20){
            loop = 0;
        }
    }
int loop;
    int prevGear = 1;
    long last_clicked;
    int min_delay = 10;
    float consumption;
    int prefSpeedTwo;

    boolean blinkrR=true;
    boolean blinkrL=true;
    boolean engineOn = true;
    boolean starterState = false;
    boolean ignitionState = false;
    boolean headLamp = false;
    boolean lowLamp = false;
    boolean handBreak = false;
    boolean horn = false;
    boolean fog_light = false;
    boolean fog_light_rear = false;
    boolean park_light = false;

    float BlinkrZ;

    float angleR;
    float prevAngleR =135;
    float angleConR =135;
    float rotationSpeedR;

    float angleO;
    float prevAngleO =135;
    float angleConO =135;
    float rotationSpeedO;

    float angleB;
    float prevAngleB =135;
    float angleConB =135;
    float rotationSpeedB;

    float angleH;
    float prevAngleH =135;
    float angleConH =135;
    float rotationSpeedH;




    long delta = SystemClock.uptimeMillis() - last_clicked;
    private void BlinkrOnOff(int i) { // 0 pro Off, ostatni pro On
       // final MediaPlayer mp = MediaPlayer.create(mActivityContext, R.raw.button);
        if (i < 500) {
            BlinkrZ = -5.1f;

            if(delta > 490){
              //  mp.start();
                last_clicked = SystemClock.uptimeMillis();
            }
        } else {
            BlinkrZ = -4.9f;

            if(delta>490){
              //  mp.start();
                last_clicked = SystemClock.uptimeMillis();
            }
        }

    }





    private void SetValueRychlost(int x){

        angleR = -135*(x-120)/120;
        if(angleR > 135){
            angleR = 135;
        }
        if(angleR < -135){
            angleR = -135;
        }
        int actualSpeed;
        actualSpeed = (int)angleConR*120/(-135)+120;

        rotationSpeedR = (abs((float)Speed - (float)actualSpeed)/40);
        if(rotationSpeedR <0.05f){
            rotationSpeedR = 0.05f;
        }

        if(angleR> angleConR & angleConR != angleR){

            angleConR = angleConR + rotationSpeedR;

        } else if(angleR < angleConR & angleConR != angleR){

            angleConR = angleConR - rotationSpeedR;

        } else{
            angleConR = angleR;

        }



    }

    private void SetValueRychlost1(int x){

       // angleR = -(121f/80f * x - 121);
        angleR = -121*(x-80)/80;
        if(angleR > 121){
            angleR = 121;
        }
        if(angleR < -121){
            angleR = -121;
        }
        int actualSpeed;
        //actualSpeed = (int)angleConR/(-121)+80;
        actualSpeed = (int)angleConR*80/(-121)+80;

        rotationSpeedR = (abs((float)Speed - (float)actualSpeed)/40);;
        if(rotationSpeedR <0.05f){
            rotationSpeedR = 0.05f;
        }


        if(angleR> angleConR & angleConR != angleR){

            angleConR = angleConR + rotationSpeedR;

        } else if(angleR < angleConR & angleConR != angleR){

            angleConR = angleConR - rotationSpeedR;

        } else{
            angleConR = angleR;

        }
    }

    private void SetValueOtacky(int x){

        angleO = -135*(x-3000)/3000;

        if(angleO > 135){
            angleO = 135;
        }
        if(angleO < -135){
            angleO = -135;
        }

        int actualSpeed;
        actualSpeed = (int)angleConO*3000/(-135)+3000;

        rotationSpeedO = (abs((float)Rpm - (float)actualSpeed)/1000);;
        if(rotationSpeedO <0.05f){
            rotationSpeedO = 0.05f;
        }

        if(angleO> angleConO & angleConO != angleO){

            angleConO = angleConO + rotationSpeedO;

        } else if(angleO < angleConO & angleConO != angleO){

            angleConO = angleConO - rotationSpeedO;

        } else{
            angleConO = angleO;

        }


    }

    private void SetValueConsumption(float x){

        angleO = -9*x;
        if(angleO < -90){
            angleO = 90;
        }
        if(angleO > 0){
            angleO = 0;
        }
        float actualSpeed;
        actualSpeed = angleConO/9;

        rotationSpeedO = (abs((float)consumption - (float)actualSpeed)/30);;
        if(rotationSpeedO <1f){
            rotationSpeedO = 1f;
        }

        if(angleO> angleConO & angleConO != angleO){

            angleConO = angleConO + rotationSpeedO;
            angleConO = (int) angleConO;
        } else if(angleO < angleConO & angleConO != angleO){

            angleConO = angleConO - rotationSpeedO;
            angleConO = (int) angleConO;

        } else{
            angleConO = angleO;
            angleConO = (int) angleConO;
        }


    }

    private void SetValueBenzin(int x){// v procentech

        angleB = -135*(x-50)/50;
        if(angleB > 135){
            angleB = 135;
        }
        if(angleB < -135){
            angleB = -135;
        }
        int actualSpeed;
        actualSpeed = (int)angleConB*50/(-135)+50;

        rotationSpeedB = (abs((float)fuelLevel - (float)actualSpeed)/60);;
        if(rotationSpeedB <0.2f){
            rotationSpeedB = 0.2f;
        }



        if(angleB> angleConB & angleConB !=angleB){

            angleConB = angleConB + rotationSpeedB;

        } else if(angleB < angleConB & angleConB !=angleB){

            angleConB = angleConB - rotationSpeedB;

        } else{
            angleConB = angleB;
        }


    }

    private void SetValueBenzin1(int x){// v procentech

        angleB = -121*(x-50)/50;
        if(angleB > 135){
            angleB = 135;
        }
        if(angleB < -135){
            angleB = -135;
        }
        int actualSpeed;
        actualSpeed = (int)angleConB*50/(-121)+50;

        rotationSpeedB = (abs((float)fuelLevel - (float)actualSpeed)/60);;
        if(rotationSpeedB <0.2f){
            rotationSpeedB = 0.2f;
        }

        if(angleB> angleConB & angleConB !=angleB){

            angleConB = angleConB + rotationSpeedB;

        } else if(angleB < angleConB & angleConB !=angleB){

            angleConB = angleConB - rotationSpeedB;

        } else{
            angleConB = angleB;
        }


    }

    private void SetValueHl(int x){

        angleH =  (float) ((-3.375*x) + (303.75));
        if(angleH > 135){
            angleH = 135;
        }
        if(angleH < -135){
            angleH = -135;
        }
        float actualSpeed;
        actualSpeed = (float)((angleConH-303.75)/(-3.375));

        rotationSpeedH = (abs((float)temperature - (float)actualSpeed)/60);;
        if(rotationSpeedH <0.2f){
            rotationSpeedH = 0.2f;
        }

        if(angleH> angleConH & angleConH !=angleH){

            angleConH = angleConH + rotationSpeedH;

        } else if(angleH < angleConH & angleConH !=angleH){

            angleConH = angleConH - rotationSpeedH;

        } else{
            angleConH = angleH;

        }
    }

    private void SetValueVoltage(int x){

        angleH =  (-121f/100f*x);
        if(angleH > 121){
            angleH = 121;
        }
        if(angleH < -121){
            angleH = -121;
        }
        float actualSpeed;
        actualSpeed = (float)(-angleConH*100/121);

        rotationSpeedH = (abs((float)voltage - (float)actualSpeed)/60);;
        if(rotationSpeedH <0.2f){
            rotationSpeedH = 0.2f;
        }

        if(angleH> angleConH & angleConH !=angleH){

            angleConH = angleConH + rotationSpeedH;

        } else if(angleH < angleConH & angleConH !=angleH){

            angleConH = angleConH - rotationSpeedH;

        } else{
            angleConH = angleH;
        }
    }
}

