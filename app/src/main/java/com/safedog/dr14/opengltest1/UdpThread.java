package com.safedog.dr14.opengltest1;

/**
 * Created by Mat78 on 05.06.2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.Semaphore;

import static android.os.SystemClock.uptimeMillis;
import static java.lang.Math.abs;

public class UdpThread extends Thread {

    String Address;
    int Port;
    DatagramSocket socket;
    public static boolean running = false;
    //OpenglActivity.UdpClientHandler handler;
    ByteBuffer ByteBuffer;
    InetAddress IAddress;
    Context mSettingsContext;

    public UdpThread(String addr, int port,Context context) {
        super();
        Address = addr;
        Port = port;
       // this.handler = handler;
        mSettingsContext = context;

    }

    private int getPort(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mSettingsContext);
        int mPort =  Integer.parseInt(prefs.getString("port_preference","5506"));
        return mPort;
    }

    private void SetRunning(boolean run){
        running = run;
    }

    public long getPacketTime(float time){
        return delta;
    }

    long delta;

    private long last_packet = 0;

    public void run() {

        while (running == true) {

            try {



                byte[] buf = new byte[64];
                socket = new DatagramSocket(Port);

                while (running == true) {

                    if(Port != getPort()){
                        Port = getPort();
                        socket.close();
                        socket = new DatagramSocket(Port);
                    }

                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    socket.receive(packet);
                 //   String line = new String(packet.getData(), 0, packet.getLength());
                 //   int size = packet.getLength();
                    delta = SystemClock.uptimeMillis() - last_packet;

                    try {
                        semaphore.acquire();
                            OrderData(buf);

                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }finally {
                            semaphore.release();
                        }


                    last_packet = SystemClock.uptimeMillis();
                }
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if (running == false) {
            socket.close();
        }
    }

    static Semaphore semaphore = new Semaphore(1);

    public int getSpeed() {
            float tmp;


            try{
                semaphore.acquire();
                tmp = speed;

            }catch (InterruptedException e){
                e.printStackTrace();
            } finally {
                semaphore.release();
                return (int) speed;

            }
    }

    public int getFuelLevel() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = fuelLevel;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) fuelLevel;

        }
    }

    public int getRpm() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = rpm;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) rpm;

        }
    }

    public int getTemperature() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = temperature;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) temperature;

        }
    }

    public int getOilLevel() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = oilLevel;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) oilLevel;

        }
    }

    public int getBreakFluid() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = breakFluid;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) breakFluid;

        }
    }

    public int getVoltage() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = voltageValue;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return (int) voltageValue;

        }
    }

    public int getGear() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = gear;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return gear;

        }
    }

    public float getDistance() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = distance;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return  distance;

        }
    }

    public float getFuelCons() {
        float tmp;


        try{
            semaphore.acquire();
            tmp = fuel_consumption;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return fuel_consumption;

        }
    }

    public boolean getLeftBlinkr() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = leftBlinkr;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return leftBlinkr;

        }
    }

    public boolean getRightBlinkr() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = rightBlinkr;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return rightBlinkr;

        }
    }

    public boolean getIgnitionState() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = ignitionState;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return ignitionState;

        }
    }

    public boolean getHeadLamp() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = headLamp;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return headLamp;

        }
    }

    public boolean getHandBreak() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = handBreak;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return handBreak;

        }
    }

    public boolean getEngineOn() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = engineOn;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return engineOn;

        }
    }

    public boolean getHorn() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = horn;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return horn;

        }
    }

    public boolean getFogLight() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = fog_light;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return fog_light;

        }
    }

    public boolean getFogLightRear() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = fog_light_rear;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return fog_light_rear;

        }
    }

    public boolean getStarterState() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = starterState;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return starterState;

        }
    }

    public boolean getLowLamp() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = lowLamp;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return lowLamp;

        }
    }

    public boolean getParkLight() {
        boolean tmp;


        try{
            semaphore.acquire();
            tmp = park_light;

        }catch (InterruptedException e){
            e.printStackTrace();
        } finally {
            semaphore.release();
            return park_light;

        }
    }

    float speed=160;
    float rpm = 2000;
    float fuelLevel = 100;
    float breakFluid = 0;
    float temperature = 90;
    float oilLevel = 0;
    float voltageValue = 0;
    float fuel_consumption = 0;
    float distance = 0;
    int gear = 0;
    long booleans=0;

    boolean engineOn = false;
    boolean rightBlinkr = true;
    boolean leftBlinkr = true;
    boolean starterState = false;
    boolean ignitionState = true;
    boolean headLamp = true;
    boolean lowLamp = false;
    boolean handBreak = true;
    boolean horn = false;
    boolean fog_light = false;
    boolean fog_light_rear = false;
    boolean park_light = true;


    int wipers = 0;
    int clutch =0;
    int accelerator=0;
    int brake=0;




    public boolean getBitN(int position)
    {
        long mask =  (0x01 << position);
        return (booleans & mask) == mask;
    }

    public void OrderData(byte[] bytes) {
        int size = bytes.length;
        ByteBuffer bb = ByteBuffer.wrap(bytes,0,size);
        bb.order(ByteOrder.LITTLE_ENDIAN); // or LITTLE_ENDIAN


        speed = bb.getFloat(0);
        rpm = bb.getFloat(4);
        fuelLevel = bb.getFloat(8);
        breakFluid = bb.getFloat(12);
        temperature = bb.getFloat(16);
        oilLevel = bb.getFloat(20);
        voltageValue = bb.getFloat(24);
        fuel_consumption = bb.getFloat(28);
        distance = bb.getFloat(32);
        gear = bb.getInt(36);

        booleans = bb.getLong(40);

        engineOn = getBitN(0);
        rightBlinkr = getBitN(1);
        leftBlinkr = getBitN(2);
        starterState = getBitN(3);
        ignitionState =getBitN(4);
        headLamp =getBitN(5);
        lowLamp = getBitN(6);
        handBreak = getBitN(7);
        horn =getBitN(8);
        fog_light = getBitN(9);
        fog_light_rear = getBitN(10);
        park_light = getBitN(11);

        wipers = bb.getInt(48);
        clutch = bb.getInt(52);
        accelerator = bb.getInt(56);
        brake = bb.getInt(60);

    }



}

