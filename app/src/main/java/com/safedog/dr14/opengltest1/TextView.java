package com.safedog.dr14.opengltest1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import com.safedog.dr14.opengltest1.helper.RawResourceReader;
import com.safedog.dr14.opengltest1.helper.ShaderHelper;
import com.safedog.dr14.opengltest1.helper.TextureHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.R.attr.id;

/**
 * Created by Mat78 on 13.06.2017.
 */

public class TextView {

    static int Tlacitko =0;
    static int loop;
    static int acc;

    /** Store our model data in a float buffer. */
    private FloatBuffer mCubePositions;
    private final FloatBuffer mCubeNormals;
    private FloatBuffer mCubeTextureCoordinates;
    private final Context mObjectContext;
    private int mTextureDataHandle1;
    private int mTextureDataHandle2;
    private int mTextureDataHandle3;
    private int mTextureDataHandleFont;

    /** This will be used to pass in the transformation matrix. */
    private int mMVPMatrixHandle;

    /** This will be used to pass in the modelview matrix. */
    private int mMVMatrixHandle;

    /** This will be used to pass in the texture. */
    private int mTextureUniformHandle;

    /** This will be used to pass in model position information. */
    private int mPositionHandle;

    /** This will be used to pass in model normal information. */
    private int mNormalHandle;

    /** This will be used to pass in model texture coordinate information. */
    private int mTextureCoordinateHandle;

    /** This is a handle to our cube shading program. */
    private int mProgramHandle;

    /**
     * Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
     * it positions things relative to our eye.
     */
    private float[] mViewMatrix = new float[16];

    /** How many bytes per float. */
    private final int mBytesPerFloat = 4;

    /** Size of the position data in elements. */
    private final int mPositionDataSize = 3;

    /** Size of the color data in elements. */
    private final int mColorDataSize = 4;

    /** Size of the normal data in elements. */
    private final int mNormalDataSize = 3;

    /** Size of the texture coordinate data in elements. */
    private final int mTextureCoordinateDataSize = 2;

    /**
     * Store the model matrix. This matrix is used to move models from object space (where each model can be thought
     * of being located at the center of the universe) to world space.
     */
    private float[] mModelMatrix = new float[16];





    /** Allocate storage for the final combined matrix. This will be passed into the shader program. */
    private float[] mMVPMatrix = new float[16];

    protected String getVertexShader()
    {
        return RawResourceReader.readTextFileFromRawResource(mObjectContext, R.raw.per_pixel_vertex_shader_tex_and_light);
    }

    protected String getFragmentShader()
    {
        return RawResourceReader.readTextFileFromRawResource(mObjectContext, R.raw.per_pixel_fragment_shader_tex_and_light);
    }
/*
    float getChar (char character){
        if (character == 'A'){ }

    }
  */

    int setCharPos (int position){

        return position;
    }

    public TextView(final Context GaugeContext){
        mObjectContext = GaugeContext;

        // Define points for a cube.



        // X, Y, Z
        // The normal is used in light calculations and is a vector which points
        // orthogonal to the plane of the surface. For a cube model, the normals
        // should be orthogonal to the points of each face.
        final float[] cubeNormalData =
                {
                        // Front face
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 1.0f,


                };

        // S, T (or X, Y)
        // Texture coordinate data.
        // Because images have a Y axis pointing downward (values increase as you move down the image) while
        // OpenGL has a Y axis pointing upward, we adjust for that here by flipping the Y axis.
        // What's more is that the texture coordinates are the same for every face.




        // Initialize the buffers.
        mCubePositions = ByteBuffer.allocateDirect(cubePositionData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubePositions.put(cubePositionData).position(0);


        mCubeNormals = ByteBuffer.allocateDirect(cubeNormalData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeNormals.put(cubeNormalData).position(0);

        mCubeTextureCoordinates = ByteBuffer.allocateDirect(cubeTextureCoordinateData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeTextureCoordinates.put(cubeTextureCoordinateData).position(0);

        // Position the eye in front of the origin.
        final float eyeX = 0.0f;
        final float eyeY = 0.0f;
        final float eyeZ = -0.5f;

        // We are looking toward the distance
        final float lookX = 0.0f;
        final float lookY = 0.0f;
        final float lookZ = -5.0f;

        // Set our up vector. This is where our head would be pointing were we holding the camera.
        final float upX = 0.0f;
        final float upY = 1.0f;
        final float upZ = 0.0f;

        // Set the view matrix. This matrix can be said to represent the camera position.
        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
        // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

        final String vertexShader = getVertexShader();
        final String fragmentShader = getFragmentShader();

        final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);
        final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);

        mProgramHandle = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,
                new String[] {"a_Position",  "a_Color", "a_Normal", "a_TexCoordinate"});
/////////////////////////

    //////////////////////////
            mTextureDataHandle1 = TextureHelper.loadTexture(mObjectContext, R.drawable.l);


            mTextureDataHandle2 = TextureHelper.loadTexture(mObjectContext, R.drawable.r);

            mTextureDataHandle3 = TextureHelper.loadTexture(mObjectContext, R.drawable.smiley);

            mTextureDataHandleFont = TextureHelper.loadTexture(mObjectContext, R.drawable.font_edit);



    }

    float[] setScale(float[] children, float scale){

            float array[] = new float[children.length];
            for( int i = 0; i < children.length; i++)
                {
                    if(children[i] != 1.0f ){
                        array[i] = children[i] * scale;
                    }
                    
                }//end for
             return array;
        }

    void setDefaultPos(){
        cubePositionData[0] = -0.0506f;
        cubePositionData[1] = 0.075f;


        cubePositionData[3] = -0.0506f;
        cubePositionData[4] = -0.075f;

        cubePositionData[7] = 0.0506f;
        cubePositionData[6] = 0.075f;

        cubePositionData[9] = -0.0506f;
        cubePositionData[10] = -0.075f;

        cubePositionData[12] = 0.0506f;
        cubePositionData[13] = -0.075f;

        cubePositionData[15] = 0.0506f;
        cubePositionData[16] = 0.075f;


    }

    // X, Y, Z
    float[] cubePositionData =
            {
                    // In OpenGL counter-clockwise winding is default. This means that when we look at a triangle,
                    // if the points are counter-clockwise we are looking at the "front". If not we are looking at
                    // the back. OpenGL has an optimization where all back-facing triangles are culled, since they
                    // usually represent the backside of an object and aren't visible anyways.

                    // Front face
                    -0.0506f, 0.075f, 1.0f,
                    -0.0506f, -0.075f, 1.0f,
                    0.0506f, 0.075f, 1.0f,
                    -0.0506f, -0.075f, 1.0f,
                    0.0506f, -0.075f, 1.0f,
                    0.0506f, 0.075f, 1.0f,


            };



    float[] cubeTextureCoordinateData =
            {

                    // Front face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    0.5f, 0.0f,
                    0.0f, 1.0f,
                    0.5f, 1.0f,
                    0.5f, 0.0f,


            };

    float u = 0f;
    float v = 0f;
    float uOffset = 1f/8f;
    float vOffset = 1f/6f;





    void setTexCoordinateData(float radek, float sloupec){

        for(int i=0; i<6 ;i++){
            for(int j=0; j<8;j++){
                if(j==sloupec & i==radek){

                    cubeTextureCoordinateData [0] = 0.0f+ (float)j*uOffset;
                    cubeTextureCoordinateData [1] = 0.0f+ (float)i*vOffset;

                    cubeTextureCoordinateData [2] = 0.0f+ (float) j*uOffset;
                    cubeTextureCoordinateData [3] = 0.0f+ (float)(i+1)*vOffset;

                    cubeTextureCoordinateData [4] = 0.0f+ (float)(j+1)*uOffset;
                    cubeTextureCoordinateData [5] = 0.0f+ (float)i*vOffset;

                    cubeTextureCoordinateData [6] = 0.0f+ (float)j*uOffset;
                    cubeTextureCoordinateData [7] = 0.0f+ (float)(i+1)*vOffset;

                    cubeTextureCoordinateData [8] = 0.0f+ (float)(j+1)*uOffset;
                    cubeTextureCoordinateData [9] = 0.0f+ (float)(i+1)*vOffset;

                    cubeTextureCoordinateData [10] = 0.0f+ (float)(j+1)*uOffset;
                    cubeTextureCoordinateData [11] = 0.0f+ (float)i*vOffset;


                }
            }
        }

    }


    float charToCoordX(char x){
        if (x == 'A'| x =='B'| x =='C'| x =='D'| x =='E'| x =='F'| x =='G'| x =='G'){return 1f;}else
        if (x == 'I'| x =='J'| x =='K'| x =='L'| x =='M'| x =='N'| x =='O'| x =='P'){return 2f;}else
        if (x == 'Q'| x =='R'| x =='S'| x =='T'| x =='U'| x =='V'| x =='W'| x =='X'){return 3f;}else
        if (x == 'Y'| x =='Z'| x =='0'| x =='1'| x =='2'| x =='3'| x =='4'| x =='5'){return 4f;}else
        if (x == '6'| x =='7'| x =='8'| x =='9'| x =='!'| x =='?'| x =='+'| x =='-'){return 5f;}
        if (x == '='| x ==':'| x =='.'| x ==','| x =='*'| x =='$'| x =='€'){return 6f;}
        else {return 6f;}
    }
    float charToCoordY(char y){
        if (y == 'A'| y =='I'| y =='Q'| y =='Y'| y =='6'| y =='='){return 1f;}else
        if (y == 'B'| y =='J'| y =='R'| y =='Z'| y =='7'| y ==':'){return 2f;}else
        if (y == 'C'| y =='K'| y =='S'| y =='0'| y =='8'| y =='.'){return 3f;}else
        if (y == 'D'| y =='L'| y =='T'| y =='1'| y =='9'| y ==','){return 4f;}else
        if (y == 'E'| y =='M'| y =='U'| y =='2'| y =='!'| y =='*'){return 5f;}else
        if (y == 'F'| y =='N'| y =='V'| y =='3'| y =='?'| y =='$'){return 6f;}else
        if (y == 'G'| y =='O'| y =='W'| y =='4'| y =='+'| y =='€'){return 7f;}else
        if (y == 'H'| y =='P'| y =='X'| y =='5'| y =='-'){return 8f;}
        else {return 8f;}
    }
    /**
     * Draws a cube.
     */
    public void drawCube(float[] mProjectionMatrix,float x,float y, char text,float scale)
    {



        //cubePositionData = setScale(cubePositionData,scale);

        setTexCoordinateData(charToCoordX(text) - 1f, charToCoordY(text) - 1f);

        // Initialize the buffers.
        mCubePositions = ByteBuffer.allocateDirect(cubePositionData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubePositions.put(cubePositionData).position(0);

        mCubeTextureCoordinates = ByteBuffer.allocateDirect(cubeTextureCoordinateData.length * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeTextureCoordinates.put(cubeTextureCoordinateData).position(0);

        // Set our per-vertex lighting program.
        GLES20.glUseProgram(mProgramHandle);

        // Set program handles for cube drawing.
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVMatrix");
        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_Texture");
        mPositionHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");
        mNormalHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Normal");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");

        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandleFont);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);
/*
        if(Tlacitko == 1) {
            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle1);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureUniformHandle, 0);

            loop++;
            if(loop > 200){
                Tlacitko = 0;
                loop = 0;
            }

        }if(Tlacitko == 2) {
        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle2);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);
        loop++;
            if(loop > 200){
            Tlacitko = 0;
                loop = 0;
            }
        }

        if(Tlacitko != 1 && Tlacitko != 2){

            // Temporary create a bitmap
            //Bitmap bmp = BitmapFactory.decodeResource(mObjectContext.getResources(), id);

            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle3);

            // Set filtering
           // GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
           // GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(mTextureUniformHandle, 0);

            // Load the bitmap into the bound texture.
           // GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

            // We are done using the bitmap so we should recycle it.
           // bmp.recycle();
        }
*/
        // draw background
        Matrix.setIdentityM(mModelMatrix, 0);


        Matrix.translateM(mModelMatrix, 0, x , y , -4.9f);
        //Matrix.rotateM(mModelMatrix, 0, angleInDegrees, 0.0f, 0.0f, 1.0f);


        // Pass in the position information
        mCubePositions.position(0);
        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
                0, mCubePositions);

        GLES20.glEnableVertexAttribArray(mPositionHandle);


        // Pass in the normal information
        mCubeNormals.position(0);
        GLES20.glVertexAttribPointer(mNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false,
                0, mCubeNormals);

        GLES20.glEnableVertexAttribArray(mNormalHandle);

        // Pass in the texture coordinate information
        mCubeTextureCoordinates.position(0);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false,
                0, mCubeTextureCoordinates);

        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

        // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);

        // Pass in the modelview matrix.
        GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);

        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);

        // Pass in the combined matrix.
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);


        // Draw the cube.
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);

        //setDefaultPos();


    }
}
