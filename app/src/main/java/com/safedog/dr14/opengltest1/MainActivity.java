package com.safedog.dr14.opengltest1;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button ButtonConnect;
    EditText EditTextPort;
    EditText EditTextAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        EditTextPort = (EditText) findViewById(R.id.EditTextPort);
        ButtonConnect = (Button) findViewById(R.id.Connect);
        EditTextAddress = (EditText) findViewById(R.id.Address);

        ButtonConnect.setOnClickListener(ButtonConnectOnClickListener);


        EditTextAddress.setText(getIPAddress(true));


        Intent GLActivity = new Intent(getApplicationContext(),OpenglActivity.class);
        GLActivity.putExtra("adress",EditTextAddress.getText().toString());
        GLActivity.putExtra("port",Integer.parseInt(EditTextPort.getText().toString()));
        startActivity(GLActivity);
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    View.OnClickListener ButtonConnectOnClickListener =
            new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                }
            };
}

